package games.snakesandladders;

import games.snakesandladders.exceptions.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class BoardTest {

    @Test
    public void testBoardGeneralFunctionality1() {
        GameBoard board = new GameBoard(16);
        board.addSnake(1, 1);
        board.addSnake(2, 2);
        board.addSnake(3, 3);
        board.addSnake(4, 4);
        board.addSnake(5, 5);

        board.addLadder(6, 9);
        board.addLadder(7, 8);
        board.addLadder(8, 7);
        board.addLadder(9, 6);
        board.addLadder(10, 5);

        int boardArrayExpected[] = {0, -1, -2, -3, -4, -5, 9, 8, 7, 6, 5, 0, 0, 0, 0, 0};
        int boardArray[] = board.getBoard();

        assertArrayEquals(boardArrayExpected, boardArray);
    }

    @Test(expected = BadSnakeException.class)
    public void testSnakeLeadingToItself() {
        GameBoard board = new GameBoard(10);
        board.addSnake(5, 0);
    }

    @Test(expected = BadLadderException.class)
    public void testLadderLeadingToItself() {
        GameBoard board = new GameBoard(10);
        board.addLadder(5, 0);
    }

    @Test(expected = BadSnakeException.class)
    public void testSnakeLeadingToSnake1() {
        GameBoard board = new GameBoard(10);
        board.addSnake(4, 1);
        board.addSnake(5, 1);
    }

    @Test(expected = BadSnakeException.class)
    public void testSnakeLeadingToSnake2() {
        GameBoard board = new GameBoard(10);
        board.addSnake(5, 1);
        board.addSnake(4, 1);
    }

    @Test(expected = BadLadderException.class)
    public void testLadderLeadingToLadder1() {
        GameBoard board = new GameBoard(10);
        board.addLadder(5, 1);
        board.addLadder(4, 1);
    }

    @Test(expected = BadLadderException.class)
    public void testLadderLeadingToLadder2() {
        GameBoard board = new GameBoard(10);
        board.addLadder(4, 1);
        board.addLadder(5, 1);
    }

    @Test(expected = BadLadderException.class)
    public void testLadderLeadingToSnake1() {
        GameBoard board = new GameBoard(10);
        board.addSnake(5, 2);
        board.addLadder(4, 1);
    }

    @Test(expected = BadSnakeException.class)
    public void testSnakeLeadingToLadder1() {
        GameBoard board = new GameBoard(10);
        board.addLadder(4, 1);
        board.addSnake(5, 2);
    }

    @Test(expected = BadSnakeException.class)
    public void testSnakeOnTopOfSnake() {
        GameBoard board = new GameBoard(10);
        board.addSnake(5, 1);
        board.addSnake(5, 2);
    }

    @Test(expected = BadLadderException.class)
    public void testLadderOnTopOfLadder() {
        GameBoard board = new GameBoard(10);
        board.addLadder(5, 1);
        board.addLadder(5, 2);
    }

    @Test(expected = BadLadderException.class)
    public void testLadderOnTopOfSnake() {
        GameBoard board = new GameBoard(10);
        board.addSnake(5, 1);
        board.addLadder(5, 1);
    }

    @Test(expected = BadSnakeException.class)
    public void testSnakeOnTopOfLadder() {
        GameBoard board = new GameBoard(10);
        board.addLadder(5, 1);
        board.addSnake(5, 1);
    }

    @Test(expected = BadBoardSizeException.class)
    public void testBadBoardSize1() {
        GameBoard board = new GameBoard(2);
    }

    @Test(expected = BadBoardSizeException.class)
    public void testBadBoardSize2() {
        GameBoard board = new GameBoard(1);
    }

    @Test(expected = BadBoardSizeException.class)
    public void testBadBoardSize3() {
        GameBoard board = new GameBoard(0);
    }

    @Test(expected = BadBoardSizeException.class)
    public void testBadBoardSize4() {
        GameBoard board = new GameBoard(-1);
    }

    @Test
    public void testReallyBigBoardSize() {
        GameBoard board = new GameBoard(65535);

        for (int i = 1; i < 32767; i++) {
            board.addSnake(i, i);
        }

        for (int i = 32768; i < 65534; i++) {
            board.addLadder(i, 65534 - i);
        }
    }
}
