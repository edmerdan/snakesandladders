package games.snakesandladders;

import games.snakesandladders.exceptions.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class DieTest {

    @Test
    public void testValidFaceCountInputs() {
        Die die = null;

        die = new Die(2);
        assertEquals(2, die.getFaceCount());

        die = new Die(3);
        assertEquals(3, die.getFaceCount());

        die = new Die(6);
        assertEquals(6, die.getFaceCount());

        die = new Die(100);
        assertEquals(100, die.getFaceCount());
    }

    @Test(expected = BadFaceCountException.class)
    public void testInvalidFaceCountInputs1() {
        Die die = new Die(1);
    }

    @Test(expected = BadFaceCountException.class)
    public void testInvalidFaceCountInputs2() {
        Die die = new Die(0);
    }

    @Test(expected = BadFaceCountException.class)
    public void testInvalidFaceCountInputs3() {
        Die die = new Die(-1);
    }

    @Test
    public void testValidRollInputs() {
        int roll;
        Die die = null;

        die = new Die(3);
        for (int i = 0; i < 100; i++) {
            roll = die.roll();
            assertTrue(roll > 0);
            assertTrue(roll < 4);
        }

        die = new Die(100);
        for (int i = 0; i < 1000; i++) {
            roll = die.roll();
            assertTrue(roll > 0);
            assertTrue(roll < 101);
        }
    }

}
