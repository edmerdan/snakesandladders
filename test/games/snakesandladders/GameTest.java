package games.snakesandladders;

import games.snakesandladders.exceptions.*;
import org.junit.Test;
import static org.junit.Assert.*;

public class GameTest {

    @Test
    public void testGeneralFunctionality1() {
        Player[] players = {
            new Player("Break1"),
            new Player("Break2"),
            new Player("Break3"),
            new Player("Break4"),
            new Player("Break5"),
            new Player("Break6")
        };

        GameBoard board = new GameBoard(100);
        board.addSnake(1, 1);
        board.addSnake(2, 2);
        board.addSnake(3, 3);
        board.addSnake(4, 4);
        board.addSnake(5, 5);

        Game game = new Game(board, players, new Die(5), new DummyGameReporter(), new Rules());
        game.startGame();

        while (game.getTurn() < 100) {
            game.nextTurn();
        }

        assertEquals(0, players[0].getPosition());
        assertEquals(0, players[1].getPosition());
        assertEquals(0, players[2].getPosition());
        assertEquals(0, players[3].getPosition());
        assertEquals(0, players[4].getPosition());
        assertEquals(0, players[5].getPosition());

        for (Player player : players) {
            player.reset();
        }

        board = new GameBoard(100);
        board.addSnake(1, 1);
        board.addSnake(2, 2);
        board.addSnake(3, 3);
        board.addSnake(4, 4);
        board.addSnake(5, 5);
        board.addLadder(6, 90);

        game = new Game(board, players, new Die(6), new DummyGameReporter(), new Rules());

        game.startGame();

        while (game.getState() != GameState.COMPLETED) {
            game.nextTurn();
        }

        assertTrue(players[game.getPlayerTurn()].getPosition() == 99);
    }

    @Test
    public void testGeneralFunctionality2() {
        Player[] players = {
            new Player("Break1"),
            new Player("Break2"),
            new Player("Break3"),
            new Player("Break4"),};
        Die die = new Die(6);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), new Rules());

        game.startGame();
        while (game.getState() != GameState.COMPLETED) {
            game.nextTurn();
        }

        game.resetGame();

        game.startGame();
        while (game.getState() != GameState.COMPLETED) {
            game.nextTurn();
        }
    }

    @Test(expected = BadPlayerCountException.class)
    public void testInvalidPlayerCount1() {
        Player[] players = {
            new Player("Break1"),};

        Die die = new Die(6);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), new Rules());
    }

    @Test(expected = BadPlayerCountException.class)
    public void testInvalidPlayerCount2() {
        Player[] players = {};

        Die die = new Die(6);
        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), new Rules());
    }

    @Test(expected = PlayingActionException.class)
    public void testInvalidPlayerState1() {
        Player[] players = {
            new Player("Break1"),
            new Player("Break2"),
            new Player("Break3"),
            new Player("Break4"),};
        players[2].startPlaying();
        Die die = new Die(6);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), new Rules());
    }

    @Test(expected = PlayingActionException.class)
    public void testInvalidPlayerState2() {
        Player[] players = {
            new Player("Break1"),
            new Player("Break2"),
            new Player("Break3"),
            new Player("Break4"),};
        players[2].startPlaying();
        players[2].quitPlaying();
        Die die = new Die(6);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), new Rules());
    }

    @Test(expected = PlayingActionException.class)
    public void testInvalidPlayerState3() {
        Player[] players = {
            new Player("Break1"),
            new Player("Break2"),
            new Player("Break3"),
            new Player("Break4"),};
        players[2].startPlaying();
        players[2].gameCompleted();
        Die die = new Die(6);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), new Rules());
    }

    @Test(expected = PlayingActionException.class)
    public void testInvalidPlayerState4() {
        Player[] players = {
            new Player("Break1"),
            new Player("Break2"),
            new Player("Break3"),
            new Player("Break4"),};
        Die die = new Die(6);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), new Rules());
        players[2].startPlaying();
        game.startGame();
    }

    @Test(expected = PlayingActionException.class)
    public void testInvalidPlayerState5() {
        Player[] players = {
            new Player("Break1"),
            new Player("Break2"),
            new Player("Break3"),
            new Player("Break4"),};
        Die die = new Die(6);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), new Rules());
        game.startGame();
        players[2].quitPlaying();

        game.nextTurn();
        game.nextTurn();
        game.nextTurn();
        game.nextTurn();
    }

    @Test(expected = PlayingActionException.class)
    public void testDoublePlayerReference() {
        Player duplicatePlayer = null;
        Player[] players = {
            duplicatePlayer = new Player("Break1"),
            new Player("Break2"),
            duplicatePlayer,
            new Player("Break4"),};
        Die die = new Die(6);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), new Rules());
        game.startGame();

        while (game.getState() != GameState.COMPLETED) {
            game.nextTurn();
        }
    }

    @Test
    public void testBackToStartRule() {
        Player[] players = {
            new Player("Break1"),
            new Player("Break2"),
            new Player("Break3"),
            new Player("Break4"),};
        LoadedDie die = new LoadedDie(6);

        Rules rules = new Rules();
        rules.setRollPassedFinishMode(Rules.RollPassedFinishMode.BACK_TO_START);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), rules);
        game.startGame();
        die.setRoll(99);
        game.nextTurn();
        die.setRoll(101);
        game.nextTurn();
        die.setRoll(102);
        game.nextTurn();
        die.setRoll(103);
        game.nextTurn();

        assertEquals(98, players[0].getPosition());
        assertEquals(0, players[1].getPosition());
        assertEquals(0, players[2].getPosition());
        assertEquals(1, players[3].getPosition());
    }

    @Test
    public void testBounceBackwardRule() {
        Player[] players = {
            new Player("Break1"),
            new Player("Break2"),
            new Player("Break3"),};
        LoadedDie die = new LoadedDie(6);

        Rules rules = new Rules();
        rules.setRollPassedFinishMode(Rules.RollPassedFinishMode.BOUNCE_BACKWARD);

        Game game = new Game(new GameBoard(100), players, die, new DummyGameReporter(), rules);
        game.startGame();
        die.setRoll(1);
        game.nextTurn();
        game.nextTurn();
        game.nextTurn();
        die.setRoll(98);
        game.nextTurn();
        die.setRoll(100);
        game.nextTurn();
        die.setRoll(101);
        game.nextTurn();

        assertEquals(98, players[0].getPosition());
        assertEquals(98, players[1].getPosition());
        assertEquals(97, players[2].getPosition());
    }

}
