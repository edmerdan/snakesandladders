package games.snakesandladders;

import org.junit.Test;
import static org.junit.Assert.*;
import games.snakesandladders.exceptions.*;

public class PlayerTest {

    @Test
    public void testUndoRoll() {
        Player player = new Player("Hello Unit Test");
        player.startPlaying();
        player.changePosition(10);
        player.changePosition(25);
        player.undoRoll();
        assertEquals(9, player.getPosition());
    }

    @Test(expected = BadLastRollException.class)
    public void testInvalidLastRoll1() {
        Player player = new Player("Hello Unit Test");
        player.startPlaying();
        player.undoRoll();
    }

    @Test(expected = BadPlayerPositionException.class)
    public void testInvalidPlayerPosition2() {
        Player player = new Player("Hello Unit Test");
        player.startPlaying();
        player.changePosition(0);
    }

    @Test(expected = BadLastRollException.class)
    public void testInvalidLastRoll3() {
        Player player = new Player("Hello Unit Test");
        player.startPlaying();
        player.changePosition(10);
        player.undoRoll();
        player.undoRoll();
    }

    @Test
    public void testReset() {
        Player player = new Player("Hello Unit Test");
        player.startPlaying();
        player.changePosition(25);
        player.quitPlaying();
        player.reset();
        assertEquals(PlayerState.UNINITIALISED, player.getState());
        assertEquals(-1, player.getPosition());
        assertEquals("Hello Unit Test", player.getName());
        player.startPlaying();
        player.changePosition(20);
        assertEquals(19, player.getPosition());
    }

    @Test(expected = PlayingActionException.class)
    public void testInvalidStates1() {
        Player player = new Player("Hello Unit Test");
        player.changePosition(10);
    }

    @Test(expected = BadPlayerPositionException.class)
    public void testInvalidPlayerPosition1() {
        Player player = new Player("Hello Unit Test");
        player.startPlaying();
        player.changePosition(-1);
    }

    @Test
    public void testPlayerStates() {
        Player player = new Player("Hello Unit Test");
        assertEquals(PlayerState.UNINITIALISED, player.getState());
        player.startPlaying();
        assertEquals(PlayerState.PLAYING, player.getState());
        player.gameCompleted();
        assertEquals(PlayerState.FINISHED, player.getState());
        player.quitPlaying();
        assertEquals(PlayerState.QUIT, player.getState());
    }

    @Test
    public void testChangePosition() {
        Player player = new Player("Hello Unit Test");
        player.startPlaying();
        player.changePosition(10);
        assertEquals(9, player.getPosition());
        player.changePosition(10);
        assertEquals(19, player.getPosition());
    }

    @Test
    public void testGetPosition() {
        Player player = new Player("Hello Unit Test");
        assertEquals(-1, player.getPosition());
    }

    @Test
    public void testGetName() {
        Player player = new Player("Hello Unit Test");
        assertEquals("Hello Unit Test", player.getName());
    }

}
