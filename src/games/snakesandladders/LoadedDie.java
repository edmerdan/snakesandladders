package games.snakesandladders;

import games.snakesandladders.exceptions.*;

public class LoadedDie extends DieAbstract {

    private final int faces;
    private int fakeRoll;

    /**
     * Create a new Die object for use with a game.
     *
     * @param faces The number of faces, must be grater than 1.
     * @throws BadFaceCountException
     */
    public LoadedDie(int faces) throws BadFaceCountException {
        if (faces <= 1) {
            throw new BadFaceCountException("Face count is too small.", null);
        }

        this.faces = faces;
        this.fakeRoll = 1;
    }

    /**
     * Get the number of faces of this Die.
     *
     * @return Faces
     */
    @Override
    public int getFaceCount() {
        return this.faces;
    }

    /**
     * Return a faked roll.
     *
     * @return
     */
    @Override
    public int roll() {
        return this.fakeRoll;
    }

    /**
     * The fake roll to use for all proceeding rolls.
     *
     * @param roll
     */
    public void setRoll(int roll) {
        this.fakeRoll = roll;
    }
}
