package games.snakesandladders.exceptions;

public class BadLastRollException extends SnakesAndLaddersException {

    public BadLastRollException(String message, Throwable error) {
        super(message, error);
    }
}
