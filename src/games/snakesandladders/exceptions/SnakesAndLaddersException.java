package games.snakesandladders.exceptions;

public class SnakesAndLaddersException extends RuntimeException {

    public SnakesAndLaddersException(String message, Throwable error) {
        super(message, error);
    }
}
