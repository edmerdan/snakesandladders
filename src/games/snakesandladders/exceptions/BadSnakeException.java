package games.snakesandladders.exceptions;

public class BadSnakeException extends SnakesAndLaddersException {

    public BadSnakeException(String message, Throwable error) {
        super(message, error);
    }
}
