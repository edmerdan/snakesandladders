package games.snakesandladders.exceptions;

public class BadLadderException extends SnakesAndLaddersException {

    public BadLadderException(String message, Throwable error) {
        super(message, error);
    }
}
