package games.snakesandladders.exceptions;

public class NonPlayingActionException extends SnakesAndLaddersException {

    public NonPlayingActionException(String message, Throwable error) {
        super(message, error);
    }
}
