package games.snakesandladders.exceptions;

public class BadFaceCountException extends SnakesAndLaddersException {

    public BadFaceCountException(String message, Throwable error) {
        super(message, error);
    }
}
