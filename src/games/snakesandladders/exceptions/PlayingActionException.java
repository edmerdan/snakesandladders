package games.snakesandladders.exceptions;

public class PlayingActionException extends SnakesAndLaddersException {

    public PlayingActionException(String message, Throwable error) {
        super(message, error);
    }
}
