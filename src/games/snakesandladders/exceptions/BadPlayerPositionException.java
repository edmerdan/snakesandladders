package games.snakesandladders.exceptions;

public class BadPlayerPositionException extends SnakesAndLaddersException {

    public BadPlayerPositionException(String message, Throwable error) {
        super(message, error);
    }
}
