package games.snakesandladders.exceptions;

public class BadBoardSizeException extends SnakesAndLaddersException {

    public BadBoardSizeException(String message, Throwable error) {
        super(message, error);
    }
}
