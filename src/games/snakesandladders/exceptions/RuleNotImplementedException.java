package games.snakesandladders.exceptions;

public class RuleNotImplementedException extends SnakesAndLaddersException {

    public RuleNotImplementedException(String message, Throwable error) {
        super(message, error);
    }
}
