package games.snakesandladders.exceptions;

public class BadPlayerCountException extends SnakesAndLaddersException {

    public BadPlayerCountException(String message, Throwable error) {
        super(message, error);
    }
}
