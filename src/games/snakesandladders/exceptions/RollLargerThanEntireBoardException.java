package games.snakesandladders.exceptions;

public class RollLargerThanEntireBoardException extends SnakesAndLaddersException {

    public RollLargerThanEntireBoardException(String message, Throwable error) {
        super(message, error);
    }
}
