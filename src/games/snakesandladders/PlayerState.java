package games.snakesandladders;

public enum PlayerState {
    UNINITIALISED,
    PLAYING,
    FINISHED,
    QUIT
}
