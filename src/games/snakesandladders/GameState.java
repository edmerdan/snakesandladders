package games.snakesandladders;

public enum GameState {
    UNINITIALISED,
    PLAYING,
    RUNNERSUP,
    COMPLETED
}
