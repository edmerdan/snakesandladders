package games.snakesandladders;

import games.snakesandladders.exceptions.*;

public class DummyGameReporter extends GameReporterAbstract {

    @Override
    public void PlayerDieRollEvent(Player player, int roll, int faceCount, int turnCount, int playerTurn) {
    }

    @Override
    public void PlayerLandOnLadderEvent(Player player, int turnCount, int playerTurn) {
    }

    @Override
    public void PlayerLandOnSnakeEvent(Player player, int turnCount, int playerTurn) {
    }

    @Override
    public void PlayerVictoryEvent(Player player, int turnCount) {
    }

    @Override
    public void PlayerLandPassedBoardEvent(Player player, int turnCount, int playerTurn, Rules.RollPassedFinishMode rollPassedFinishMode) throws RuleNotImplementedException {
    }
}
