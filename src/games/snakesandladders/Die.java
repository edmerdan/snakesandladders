package games.snakesandladders;

import games.snakesandladders.exceptions.*;
import java.security.SecureRandom;

public class Die extends DieAbstract {

    private final int faces;
    /**
     * SecureRandom is used over Random to avoid non-uniform distribution and
     * bias.
     */
    private final SecureRandom random;

    /**
     * Create a new Die object for use with a game.
     *
     * @param faces The number of faces, must be grater than 1.
     * @throws BadFaceCountException
     */
    public Die(int faces) throws BadFaceCountException {
        if (faces <= 1) {
            throw new BadFaceCountException("Face count is too small.", null);
        }

        this.random = new SecureRandom();
        this.faces = faces;
    }

    /**
     * Get the number of faces of this Die.
     *
     * @return Faces
     */
    @Override
    public int getFaceCount() {
        return this.faces;
    }

    /**
     * Roll the Die, result is one through number of faces inclusive.
     *
     * @return
     */
    @Override
    public int roll() {
        return this.random.nextInt(faces) + 1;
    }
}
