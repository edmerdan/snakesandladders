package games.snakesandladders;

import games.snakesandladders.exceptions.*;

abstract public class DieAbstract {

    public DieAbstract() {
    }

    public DieAbstract(int faces) throws BadFaceCountException {
    }

    abstract public int getFaceCount();

    abstract public int roll();
}
