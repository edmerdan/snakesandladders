package games.snakesandladders;

import games.snakesandladders.exceptions.*;

public class Game {

    private final GameBoard board;
    private final Player[] players;
    private int playerTurn;
    private int turnCount;
    private GameState state;
    private final DieAbstract die;
    private final GameReporterAbstract reporter;
    private final Rules rules;

    /**
     * Create new Game instance.
     *
     * @param board The GameBoard to use.
     * @param players Array of Player objects.
     * @param die The Die which will be used for player rolls.
     * @param reporter The GameReporter to use.
     * @param rules The rules of the game to use.
     * @throws BadBoardSizeException
     * @throws BadPlayerCountException
     * @throws PlayingActionException
     */
    public Game(GameBoard board, Player[] players, DieAbstract die, GameReporterAbstract reporter, Rules rules) throws BadBoardSizeException, BadPlayerCountException, PlayingActionException {
        if (players.length < 2) {
            throw new BadPlayerCountException("Player count is too small.", null);
        }

        this.state = GameState.UNINITIALISED;
        this.die = die;
        this.board = board;
        this.reporter = reporter;
        this.rules = rules;
        this.playerTurn = 0;
        this.turnCount = 0;

        this.validatePlayers(players);
        this.players = players;
    }

    /**
     * Verify the PlayerState of every input player before using them.
     *
     * @param players
     * @throws PlayingActionException
     */
    private void validatePlayers(Player[] players) throws PlayingActionException {
        for (Player player : players) {
            if (player.getState() != PlayerState.UNINITIALISED) {
                throw new PlayingActionException("Attempting to add an initialised player to the game.", null);
            }
        }
    }

    /**
     * Resets the game and every play so it can be started again.
     */
    public void resetGame() {
        this.state = GameState.UNINITIALISED;
        this.playerTurn = 0;
        this.turnCount = 0;

        for (Player player : this.players) {
            player.reset();
        }
    }

    /**
     * Starts the game and sets up each player for the game.
     *
     * @throws PlayingActionException
     */
    public void startGame() throws PlayingActionException {
        if (this.state != GameState.UNINITIALISED) {
            throw new PlayingActionException("The game is in progress or already completed.", null);
        }

        this.state = GameState.PLAYING;

        for (Player player : players) {
            player.startPlaying();
        }
    }

    /**
     * Gets the current GameState of the board.
     *
     * @return
     */
    public GameState getState() {
        return this.state;
    }

    /**
     * Returns the number of full turns which have taken place. One full turn is
     * after each player has taken their turn once.
     *
     * @return
     */
    public int getTurn() {
        return this.turnCount;
    }

    /**
     * Returns the index of the current player's turn.
     *
     * @return
     */
    public int getPlayerTurn() {
        return this.playerTurn;
    }

    /**
     * Advances the game by one player's turn.
     *
     * @throws PlayingActionException
     * @throws BadLastRollException
     * @throws BadPlayerPositionException
     */
    public void nextTurn() throws PlayingActionException, BadLastRollException, BadPlayerPositionException, RuleNotImplementedException, RollLargerThanEntireBoardException {
        if (this.state != GameState.PLAYING) {
            throw new PlayingActionException("Attempting to take a turn when the game isn't in session.", null);
        }

        Player player = this.players[playerTurn];

        /* TODO: Actually implement this
        if (player.getState() == PlayerState.FINISHED) {
            throw new RuleNotImplementedException("PlayerState.FINISHED not implemented", null);
            System.out.printf("[Turn %d] Skipping player %s; they already hit the finish line.\n", this.turnCount, player.getName());
            this.endTurn();
            return;
        }
        
        if (player.getState() == PlayerState.QUIT) {
            throw new RuleNotImplementedException("PlayerState.QUIT not implemented", null);
            System.out.printf("[Turn %d] Skipping player %s; they quit the game early.\n", this.turnCount, player.getName());
            this.endTurn();
            return;
        }
         */
        int roll = this.die.roll();
        player.changePosition(roll);

        this.reporter.PlayerDieRollEvent(player, roll, this.die.getFaceCount(), this.turnCount, this.playerTurn);

        if (player.getPosition() >= this.board.getBoard().length) {
            switch (this.rules.getRollPassedFinishMode()) {
                case UNDO_PREVIOUS_ROLL:
                    player.undoRoll();
                    break;
                case BACK_TO_START:
                    player.undoRoll();
                    int newPosition = player.getPosition() + roll % this.board.getBoard().length;
                    if (player.getPosition() == -1 && newPosition == 0) {
                        newPosition = 1;
                    }
                    player.changePosition(newPosition);
                    break;
                case BOUNCE_BACKWARD:
                    if (roll >= this.board.getBoard().length * 2) {
                        throw new RollLargerThanEntireBoardException("Bouncing back from such a large roll is not supported.", null);
                    }
                    player.undoRoll();
                    player.changePosition((this.board.getBoard().length - 1) - ((player.getPosition() + roll % this.board.getBoard().length) + 1));
                    break;
                default:
                    throw new RuleNotImplementedException("Unknown rule: " + this.rules.getRollPassedFinishMode(), null);
            }

            this.reporter.PlayerLandPassedBoardEvent(player, this.turnCount, this.playerTurn, this.rules.getRollPassedFinishMode());
        }

        int movementDirection = player.getPosition() < 0 ? 0 : this.board.getBoard()[player.getPosition()];

        if (movementDirection > 0) {
            player.changePosition(this.board.getBoard()[player.getPosition()]);
            this.reporter.PlayerLandOnLadderEvent(player, turnCount, playerTurn);
        }

        if (movementDirection < 0) {
            player.changePosition(this.board.getBoard()[player.getPosition()]);
            this.reporter.PlayerLandOnSnakeEvent(player, turnCount, playerTurn);
        }

        if (player.getPosition() == this.board.getBoard().length - 1) {
            player.gameCompleted();
            this.state = GameState.COMPLETED;
            this.reporter.PlayerVictoryEvent(player, this.turnCount);
            return;
        }

        this.endTurn();
    }

    /**
     * Helper method to signify the end of a turn.
     */
    private void endTurn() {
        this.playerTurn++;
        if (this.playerTurn == this.players.length) {
            this.playerTurn = 0;
            this.turnCount++;
        }
    }
}
