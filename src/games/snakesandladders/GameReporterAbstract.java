package games.snakesandladders;

import games.snakesandladders.exceptions.*;

public abstract class GameReporterAbstract {

    public void PlayerDieRollEvent(Player player, int roll, int faceCount, int turnCount, int playerTurn) {
    }

    public void PlayerLandOnLadderEvent(Player player, int turnCount, int playerTurn) {
    }

    public void PlayerLandOnSnakeEvent(Player player, int turnCount, int playerTurn) {
    }

    public void PlayerLandPassedBoardEvent(Player player, int turnCount, int playerTurn, Rules.RollPassedFinishMode rollPassedFinishMode) throws RuleNotImplementedException {
    }

    public void PlayerVictoryEvent(Player player, int turnCount) {
    }
}
