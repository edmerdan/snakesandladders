package games.snakesandladders;

import games.snakesandladders.exceptions.*;

public class TextualGameReporter extends GameReporterAbstract {

    @Override
    public void PlayerDieRollEvent(Player player, int roll, int faceCount, int turnCount, int playerTurn) {
        System.out.printf("[Turn %d, Player %d] %s rolls a %d on the d%d, and advances to %d!\n", turnCount + 1, playerTurn + 1, player.getName(), roll, faceCount, player.getPosition() + 1);
    }

    @Override
    public void PlayerLandOnLadderEvent(Player player, int turnCount, int playerTurn) {
        System.out.printf("[Turn %d, Player %d] %s lands on a ladder and climbs up to %d!\n", turnCount + 1, playerTurn + 1, player.getName(), player.getPosition() + 1);
    }

    @Override
    public void PlayerLandOnSnakeEvent(Player player, int turnCount, int playerTurn) {
        System.out.printf("[Turn %d, Player %d] Oops! %s lands on a snake and runs away down to %d.\n", turnCount + 1, playerTurn + 1, player.getName(), player.getPosition() + 1);
    }

    @Override
    public void PlayerVictoryEvent(Player player, int turnCount) {
        System.out.printf("We have a winner!! %s landed on the finish line after %d turns!\n", player.getName(), turnCount + 1);
    }

    @Override
    public void PlayerLandPassedBoardEvent(Player player, int turnCount, int playerTurn, Rules.RollPassedFinishMode rollPassedFinishMode) throws RuleNotImplementedException {
        switch (rollPassedFinishMode) {
            case UNDO_PREVIOUS_ROLL:
                System.out.printf("[Turn %d, Player %d] Oops! %s rolled passed the end of the board! Their roll is undone and they return to %d.\n", turnCount + 1, playerTurn + 1, player.getName(), player.getPosition() + 1);
                break;
            case BACK_TO_START:
                System.out.printf("[Turn %d, Player %d] Oops! %s rolled passed the end of the board! They return to the start of the board and advance to %d.\n", turnCount + 1, playerTurn + 1, player.getName(), player.getPosition() + 1);
                break;
            default:
                throw new RuleNotImplementedException("Unknown rule: " + rollPassedFinishMode.toString(), null);
        }
    }
}
