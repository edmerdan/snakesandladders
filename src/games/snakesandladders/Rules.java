package games.snakesandladders;

public class Rules {

    private boolean allowRunnersUp;
    private boolean allowPlayerQuitting;
    private boolean allowPlayerLateJoining;
    private RollPassedFinishMode rollPassedFinishAction;

    public static enum RollPassedFinishMode {
        UNDO_PREVIOUS_ROLL,
        BACK_TO_START,
        BOUNCE_BACKWARD,
    }

    public Rules() {
        this.allowRunnersUp = false;
        this.allowPlayerQuitting = false;
        this.allowPlayerLateJoining = false;
        this.rollPassedFinishAction = Rules.RollPassedFinishMode.UNDO_PREVIOUS_ROLL;
    }

    public void allowRunnersUp(boolean value) {
        this.allowRunnersUp = value;
    }

    public boolean getAllowRunnersUp() {
        return this.allowRunnersUp;
    }

    public void allowPlayerQuitting(boolean value) {
        this.allowPlayerQuitting = value;
    }

    public boolean getAllowPlayerQuitting() {
        return this.allowPlayerQuitting;
    }

    public void allowPlayerLateJoining(boolean value) {
        this.allowPlayerLateJoining = value;
    }

    public boolean getAllowPlayerLateJoining() {
        return this.allowPlayerLateJoining;
    }

    public void setRollPassedFinishMode(RollPassedFinishMode mode) {
        this.rollPassedFinishAction = mode;
    }

    public RollPassedFinishMode getRollPassedFinishMode() {
        return this.rollPassedFinishAction;
    }
}
