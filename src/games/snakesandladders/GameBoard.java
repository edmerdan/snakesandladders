package games.snakesandladders;

import games.snakesandladders.exceptions.*;

public class GameBoard {

    private int[] board;

    /**
     * Create new GameBoard instance.
     *
     * @param boardSize The size of the board in number of squares.
     * @throws BadBoardSizeException
     * @throws BadPlayerCountException
     * @throws PlayingActionException
     */
    public GameBoard(int boardSize) throws BadBoardSizeException {
        if (boardSize <= 2) {
            throw new BadBoardSizeException("Board is too small.", null);
        }

        this.board = new int[boardSize];
    }

    /**
     * Add a new ladder to the game board. Must be called before game is
     * started.
     *
     * @param position The board position the ladder, starting at 0.
     * @param moveForwardCount The number of spaces to advance the player if
     * they land on this position.
     * @throws BadLadderException
     */
    public void addLadder(int position, int moveForwardCount) throws BadLadderException {
        if (moveForwardCount == 0) {
            throw new BadLadderException("A ladder that doesn't go anywhere doesn't make sense.", null);
        }

        if (moveForwardCount < 0) {
            throw new BadLadderException("Move count musn't be negative.", null);
        }

        if (position == this.board.length - 1) {
            throw new BadLadderException("A ladder cannot be at the end of the board.", null);
        }

        if (position < 0 || position >= this.board.length) {
            throw new BadLadderException("Ladder position is outside of the board.", null);
        }

        if (position + moveForwardCount >= this.board.length) {
            throw new BadLadderException("Move forward count is outside of the board.", null);
        }

        if (this.board[position] != 0) {
            throw new BadLadderException("Attempting to create a ladder on top of an existing snake or ladder.", null);
        }

        if (this.board[position + moveForwardCount] != 0) {
            throw new BadLadderException("Attempting to create a ladder pointing to another ladder or snake.", null);
        }

        for (int i = 0; i < this.board.length; i++) {
            if (i != position && i + this.board[i] == position) {
                throw new BadLadderException("Attempting to create a ladder which is pointed to by another snake or ladder.", null);
            }
        }

        this.board[position] = moveForwardCount;
    }

    /**
     * Add a new ladder to the game board. Must be called before game is
     * started.
     *
     * @param position The board position the snake, starting at 0.
     * @param moveBackwardCount The number of spaces to move the player back if
     * they land on this position.
     * @throws BadSnakeException
     */
    public void addSnake(int position, int moveBackwardCount) throws BadSnakeException {
        if (moveBackwardCount == 0) {
            throw new BadSnakeException("A snake that doesn't go anywhere doesn't make sense.", null);
        }

        if (moveBackwardCount < 0) {
            throw new BadSnakeException("Move count musn't be negative.", null);
        }

        if (position == this.board.length - 1) {
            throw new BadSnakeException("A snake cannot be at the end of the board.", null);
        }

        if (position < 0 || position >= this.board.length) {
            throw new BadSnakeException("Snake position is outside of the board.", null);
        }

        if (position - moveBackwardCount < 0) {
            throw new BadSnakeException("Move back count is outside of the board.", null);
        }

        if (this.board[position] != 0) {
            throw new BadSnakeException("Attempting to create a snake on top of an existing snake or ladder.", null);
        }

        if (this.board[position - moveBackwardCount] != 0) {
            throw new BadSnakeException("Attempting to create a snake pointing to another snake or ladder.", null);
        }

        for (int i = 0; i < this.board.length; i++) {
            if (i != position && i + this.board[i] == position) {
                throw new BadSnakeException("Attempting to create a snake which is pointed to by another snake or ladder.", null);
            }
        }

        this.board[position] = -moveBackwardCount;
    }

    /**
     * Gets the board array.
     *
     * @return board
     */
    public int[] getBoard() {
        return this.board;
    }
}
