package games.snakesandladders;

import games.snakesandladders.exceptions.*;

// TODO: Replace (or add) bounce rather than undo roll
// TODO: State for PLAYING_ON_BOARD and PLAYING_OFF_BOARD, to avoid using initial position of -1
public class Player {

    private final String name;
    private PlayerState state;
    private int position;
    private int lastRoll;

    /**
     * Create a new player for the GameBoard.
     *
     * @param name The name of this player.
     */
    public Player(String name) {
        this.name = name;
        this.state = PlayerState.UNINITIALISED;
        this.position = -1;
        this.lastRoll = 0;
    }

    /**
     * Undo a previous roll; for when a roll goes beyond the allowed values.
     *
     * @throws BadLastRollException
     */
    public void undoRoll() throws BadLastRollException {
        if (this.lastRoll <= 0) {
            throw new BadLastRollException("Attempting to undo a roll that never happened", null);
        }
        this.position -= this.lastRoll;
        this.lastRoll = 0;
    }

    /**
     * Reset the player back to default values to reuse in a new game.
     */
    public void reset() {
        this.state = PlayerState.UNINITIALISED;
        this.lastRoll = 0;
        this.position = -1;
    }

    /**
     * Set the player's state to PLAYING.
     */
    public void startPlaying() {
        if (this.state == PlayerState.PLAYING) {
            throw new PlayingActionException("Attempting to put a player into play who is already in play.", null);
        }

        this.state = PlayerState.PLAYING;
    }

    /**
     * Set the player's state to QUIT.
     */
    public void quitPlaying() {
        this.state = PlayerState.QUIT;
    }

    /**
     * Set the player's state to FINISHED.
     */
    public void gameCompleted() {
        this.state = PlayerState.FINISHED;
    }

    /**
     * Manually set an arbitrary player state.
     *
     * @param state PlayerState enum.
     */
    private void setState(PlayerState state) {
        this.state = state;
    }

    /**
     * Get the current PlayerState
     *
     * @return PlayerState
     */
    public PlayerState getState() {
        return this.state;
    }

    /**
     * Set the player's position.
     *
     * @param position New position of the player.
     * @throws BadPlayerPositionException
     */
    private void setPosition(int position) throws BadPlayerPositionException {
        if (position < 0) {
            throw new BadPlayerPositionException("Position cannot be negative.", null);
        }

        this.position = position;
    }

    /**
     * Change the position of the player forward or backward by the number of
     * places.
     *
     * @param places Positive or negative to move forward or backward.
     * @throws BadPlayerPositionException
     * @throws PlayingActionException
     */
    public void changePosition(int places) throws BadPlayerPositionException, PlayingActionException {
        if (this.state != PlayerState.PLAYING) {
            throw new PlayingActionException("Attempting to change player position while not playing a game.", null);
        }

        this.lastRoll = places > 0 ? places : 0;
        this.setPosition(this.position + places);
    }

    /**
     * Get the current position of the player.
     *
     * @return
     */
    public int getPosition() {
        return this.position;
    }

    /**
     * Get the player's name.
     *
     * @return Name
     */
    public String getName() {
        return this.name;
    }
}
