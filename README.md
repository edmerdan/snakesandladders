Computer Science I Group 2

# TODO:

* Unit testing for Rules class options; within Game unit test.
* Additional unit testing for edge cases with differing game rules.
* Implement additional game rules. Runners-up, players quitting/joining late.